﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static bool firstPlay;

    //public static List<bool> levelsUnlocked = new List<bool>();
    public GameObject[] Findables;
    public static float findablesFound;
    bool allInactive;

    public Text findableText;

    public GameObject otherAudioObject;
    public SceneLoader sceneLoader;
    public AudioClip findableSound;

    public static bool levelComplete;

    // Use this for initialization
    void Start()
    {

        if (findableText != null)
            findableText = findableText.GetComponent<Text>();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            SceneManager.LoadScene(0);
        }

        //if (SceneManager.GetActiveScene().buildIndex != 0)
        //{
        //    if(levelsUnlocked[SceneManager.GetActiveScene().buildIndex] != true && levelsUnlocked[SceneManager.GetActiveScene().buildIndex] != null)
        //    {
        //        levelsUnlocked.Add(false);
        //        levelsUnlocked[SceneManager.GetActiveScene().buildIndex] = true;
        //    }
        //}
    }

    private void OnEnable()
    {
    }

    private void OnDisable()
    {
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {
        print("Pressed");

    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        if (findableText != null)
            findableText.text = findablesFound + "/" + Findables.Length;

        //prints out and sets win condition once all findables found
        if (levelComplete)
        {
            if (SceneManager.GetActiveScene().buildIndex != 13)
            {
                StartCoroutine("LoadWait");
            }
            else
            {
                sceneLoader.LoadScene(0);
            }

        }

        //draws ray on click
        //if object is clickable, activate click effect
        if (Input.GetMouseButtonDown(0))
        {
            // TappedHandler();
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, 1000.0f))
            {
                if (hit.transform.parent.GetComponent<Clickable>() != null)
                {
                    hit.transform.parent.GetComponent<Clickable>().Clicked();
                }

                if (hit.transform.GetComponent<Clickable>() != null)
                {
                    hit.transform.GetComponent<Clickable>().Clicked();
                }

                //if the object is a findable, play a sound and activate findable action
                if (hit.transform != null)
                {
                    if (hit.transform.parent != null && hit.transform.parent.tag == "Findables")
                    {
                        GameManager.findablesFound++;
                        otherAudioObject.GetComponent<AudioSource>().PlayOneShot(findableSound);
                        Destroy(hit.transform.parent.gameObject);
                    }

                    if (hit.transform.name == "Portal")
                    {
                        sceneLoader.LoadScene(hit.transform.GetComponent<Portal>().sceneToLoad);
                    }


                }
            }
        }

    }

    IEnumerator LoadWait()
    {
        yield return new WaitForSeconds(1);
        sceneLoader.LoadScene(sceneLoader.sceneToLoad);
    }
}
