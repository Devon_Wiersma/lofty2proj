﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFade : MonoBehaviour
{

    public float fadeRate = 000.1f;
    public int fadeTime;
    public bool fadeOutOnStart;
    public bool fadeInOnStart;
    bool fadingOut;
    bool fadingIn;
    // Use this for initialization
    void Start()
    {
        if (fadeOutOnStart) {
            FadeOut(fadeTime);
        }
        if (fadeInOnStart)
        {
            FadeIn(fadeTime);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(fadingIn)
        GetComponent<CanvasGroup>().alpha += fadeRate;

        if(fadingOut)
        GetComponent<CanvasGroup>().alpha -= fadeRate;
    }

    public void FadeOut(int seconds)
    {
        StartCoroutine("FadeOutTimed", seconds);

    }

    public void FadeIn(int seconds)
    {
        StartCoroutine("FadeInTimed", seconds);
    }

    IEnumerator FadeInTimed(int fadeTime)
    {
        yield return new WaitForSeconds(fadeTime);
        fadingIn = true;
    }

    IEnumerator FadeOutTimed(int fadeTime)
    {
        yield return new WaitForSeconds(fadeTime);
        fadingOut = true;
    }
}
