﻿using UnityEngine;


public class Rotate : MonoBehaviour
{
    GameObject cameraObject;
    bool findable;

    void Start()
    {
        cameraObject = GameObject.Find("Camera");
    }

    private void Update()
    {
        Vector3 vectorToCamera = cameraObject.transform.position - gameObject.transform.position;
        transform.forward = new Vector3(vectorToCamera.x, 0f, vectorToCamera.z); 
    }
}

