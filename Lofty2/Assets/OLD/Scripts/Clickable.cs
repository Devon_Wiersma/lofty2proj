﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clickable : MonoBehaviour
{

    public GameObject nextState;

    public GameObject audioObject;
    public ParticleSystem partSys;
    public int partCount;
    public AudioClip clip;


    // Use this for initialization
    void Start()
    {
        audioObject = GameObject.Find("Potion Sound");
    }



    public void Clicked()
    {
        if (clip != null)
        {
            audioObject.GetComponent<AudioSource>().PlayOneShot(clip);
        }

        if (partSys != null)
            partSys.Emit(partCount);

        if (nextState != null)
        {
            nextState.SetActive(true);
            Destroy(gameObject);
        }
    }
}
