﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : MonoBehaviour {

    public int sceneToLoad;
    public SceneLoader sceneLoader;

    private void OnMouseDown()
    {
        sceneLoader.LoadScene(sceneToLoad);
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
