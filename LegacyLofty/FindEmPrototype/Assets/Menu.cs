﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject creditsUI;
    public GameObject menuGame;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void EnableCredits()
    {
        creditsUI.SetActive(true);
        menuGame.SetActive(false);
    }

    public void DisableCredits()
    {
        creditsUI.SetActive(false);
        menuGame.SetActive(true);
    }

    public void BeginGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
