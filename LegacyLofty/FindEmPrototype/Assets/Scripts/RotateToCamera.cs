﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateToCamera : MonoBehaviour
{
    GameObject cameraObject;

    void Start()
    {
        cameraObject = GameObject.Find("Camera");
    }

    void Update()
    {
        transform.LookAt(cameraObject.transform.position, -Vector3.up);

    }

}