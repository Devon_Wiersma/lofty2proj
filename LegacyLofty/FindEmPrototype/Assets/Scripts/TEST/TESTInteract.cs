﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;

public class TESTInteract : MonoBehaviour {

    private TapGesture tapGesture;

    // Use this for initialization
    void Start () {

    }

    private void OnEnable()
    {
        tapGesture = GetComponent<TapGesture>();
        tapGesture.Tapped += tappedHandler;
    }

    private void OnDisable()
    {
        tapGesture.Tapped += tappedHandler;
    }

    private void tappedHandler(object sender, System.EventArgs e)
    {
        print("TAPPED");
    }


    // Update is called once per frame
    void Update () {
        if(Application.platform == RuntimePlatform.Android)
        {

        }




        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.LinuxPlayer)
        {

        }
    }
}
