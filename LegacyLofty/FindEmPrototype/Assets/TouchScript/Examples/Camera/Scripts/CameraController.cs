﻿/*
 * @author Valentin Simonov / http://va.lent.in/
 */

using UnityEngine;
using TouchScript.Gestures.TransformGestures;

namespace TouchScript.Examples.CameraControl
{
    /// <exclude />
    public class CameraController : MonoBehaviour
    {
        public ScreenTransformGesture OneFingerRotate;
        public ScreenTransformGesture ManipulationGesture;
        public float PanSpeed;
        public float RotationSpeed;
        public float ZoomSpeed;
        public float maxZoom = 15;
        public float minZoom = 5;
        float newValue;

        float pitchRotation;
        float yawRotation;

        public GameObject pivot;
        public GameObject cam;



        private void OnEnable()
        {
            //activate touch handler events to control mobile controls
            OneFingerRotate.Transformed += oneFingerTransformHandler;
            ManipulationGesture.Transformed += manipulationTransformedHandler;
        }

        private void OnDisable()
        {
            //disable touch handler events
            OneFingerRotate.Transformed -= oneFingerTransformHandler;
            ManipulationGesture.Transformed -= manipulationTransformedHandler;
        }

        private void manipulationTransformedHandler(object sender, System.EventArgs e)
        {

            //Zooms in when the user spreads gesture apart
            if (newValue > (ManipulationGesture.ActivePointers[0].Position - ManipulationGesture.ActivePointers[1].Position).magnitude)
            {
                //cam.GetComponent<Camera>().orthographicSize = Mathf.Lerp(cam.GetComponent<Camera>().orthographicSize, -5, ZoomSpeed * Time.deltaTime);
                cam.GetComponent<Camera>().orthographicSize += ZoomSpeed * Time.deltaTime;
            }

            //Zooms out when user pulls gesture together
            if (newValue < (ManipulationGesture.ActivePointers[0].Position - ManipulationGesture.ActivePointers[1].Position).magnitude)
            {
                //cam.GetComponent<Camera>().orthographicSize = Mathf.Lerp(cam.GetComponent<Camera>().orthographicSize, 5, ZoomSpeed * Time.deltaTime);
                cam.GetComponent<Camera>().orthographicSize -= ZoomSpeed * Time.deltaTime;
            }

            //lock user's zoom levels to bounds to prevent them going too close/too far
            if (cam.GetComponent<Camera>().orthographicSize < maxZoom)
            {
                cam.GetComponent<Camera>().orthographicSize = maxZoom;
            }
            if (cam.GetComponent<Camera>().orthographicSize > minZoom)
            {
                cam.GetComponent<Camera>().orthographicSize = minZoom;
            }

            //set value to the new zoom level
            newValue = (ManipulationGesture.ActivePointers[0].Position - ManipulationGesture.ActivePointers[1].Position).magnitude;
        }

        //rotate the camera around the pivot point (MOBILE)
        private void oneFingerTransformHandler(object sender, System.EventArgs e)
        {
            pitchRotation = -OneFingerRotate.DeltaPosition.y / Screen.width * RotationSpeed;
            yawRotation = OneFingerRotate.DeltaPosition.x / Screen.height * RotationSpeed;

            pivot.transform.eulerAngles += new Vector3(pitchRotation, 0, 0);
            pivot.transform.eulerAngles += new Vector3(0, yawRotation, 0);
        }

    }
}